<?php

define('HOST','localhost'); 
define('USER','root');
define('PASS','');
define('DBNAME','databasetpfinal');


class Conexion
{

    protected $conexion;

    public function conectar()
    {
        $this->conexion = mysqli_connect(HOST, USER, PASS, DBNAME);
        if (!$this->conexion ) DIE("Lo sentimos, no se ha podido conectar con MySQL: " . mysql_error());

        return $this->conexion;

    }

    public function desconectar()
    {
        if ($this->conexion) {
            mysqli_close($this->conexion);
        }

    }


}