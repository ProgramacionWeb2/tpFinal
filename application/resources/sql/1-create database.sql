
-- create schema databaseTPFinal;
CREATE DATABASE IF NOT EXISTS databaseTPFinal;

use databaseTPFinal;

DROP TABLE IF EXISTS rol_permisos;
DROP TABLE IF EXISTS usuario;
DROP TABLE IF EXISTS rol;
DROP TABLE IF EXISTS permiso;

CREATE TABLE rol (
id_rol int primary key AUTO_INCREMENT,
nombre varchar(50)
);

CREATE TABLE permiso (
id_permiso int primary key AUTO_INCREMENT,
descripcion varchar(50)
);

CREATE TABLE rol_permisos (
id_rol int,
id_permiso int ,
PRIMARY KEY(id_rol, id_permiso),
FOREIGN KEY (id_rol) REFERENCES rol(id_rol),
FOREIGN KEY (id_permiso) REFERENCES permiso(id_permiso)
);

CREATE TABLE usuario (
id_usuario int primary key AUTO_INCREMENT,
dni varchar(50),
cuil varchar(50),
nombre varchar(100),
apellido varchar(100),
direccion varchar(100),
email varchar(100) not null UNIQUE,
contrasenia varchar(100),
id_rol int,
FOREIGN KEY (id_rol) REFERENCES rol(id_rol), 
id_comercio int, 
activo BOOLEAN
);

insert into rol (nombre) values
('Administrador'),
('Comerciante'),
('Repartidor'),
('Cliente');

insert into usuario (id_usuario, dni, cuil, nombre, apellido, direccion, email, contrasenia, id_rol, id_comercio, activo) values
(0, '', '', 'admin', 'admin', '', 'admin', SHA2('admin',224), 1, null, 1),
(1, '34123456', '23341234569', 'Ariel', 'Martin', 'Rivadavia 456,CABA', 'ariel@gmail.com', SHA2('1234',224), 2, 1, 1),
(2, '36123456', '23361234569', 'Tomás', 'Gómez', 'Av. Corrientes 1678, CABA', 'tomas@gmail.com', SHA2('1234',224), 2, 2, 1),
(3, '35123456', '23351234569', 'Diego', 'Salas', 'Av. Callao 765, CABA', 'diego@gmail.com', SHA2('1234',224), 3, null, 1),
(4, '37123456', '27371234569', 'Reyna', 'Rondosa', 'Florida 437, CABA', 'reyna@gmail.com', SHA2('1234',224), 4, null, 1);

-- select * from usuario;

-- select u.dni dni, u.cuil cuil, u.nombre nombre, u.apellido apellido, u.direccion direccion, r.nombre rol  from usuario u join rol r on r.id_rol = u.id_rol where u.email = 'ariel@gmail.com' and u.contrasenia = SHA2('1234',224);

-- SELECT dni, cuil, nombre, apellido, direccion, id_rol FROM usuario where email = 'tomas@gmail.com'